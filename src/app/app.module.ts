import { NgModule, Pipe } from '@angular/core';
import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { ApiService } from './Services/API/api.service'
import { Auth } from './Services/Guard/guard.service'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ChartsModule } from 'ng2-charts';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BnNgIdleService } from 'bn-ng-idle';
import { PortalModule } from '@angular/cdk/portal';
import { PopoutService } from './Pages/Shared/Service/popout.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { DatePipe } from '@angular/common';
import {NgxPrintModule} from 'ngx-print';

import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { IgxGridModule, IgxMaskModule, IgxComboModule, IgxDropDownModule, IgxSelectModule, IgxExpansionPanelModule, IgxCheckboxModule, IgxIconModule, IgxInputGroupModule, IgxButtonModule, IgxRippleModule, IgxDatePickerModule, IgxTimePickerModule } from "igniteui-angular";

// Material UI Modules
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import ResizeObserver from 'resize-observer-polyfill'
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { CKEditorModule } from 'ckeditor4-angular';
import { LoginComponent } from './Pages/Login/login.component';
import { LeftmenuComponent } from './Pages/Shared/LeftMenu/leftmenu.component';
import { TopbarComponent } from './Pages/Shared/TopBar/topbar.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ServiceComponent } from './Pages/Services/service.component';
import { CategoryComponent } from './Pages/Category/category.component';
import { GeneralComponent } from './Pages/General/general.component';
import { ClientComponent } from './Pages/Clients/client.component';
import { SubcategoryComponent } from './Pages/SubCategory/subcategory.component';
import { NewsComponent } from './Pages/News/news.component';
import { CertificationComponent } from './Pages/Certification/certification.component';
import { SubserviceComponent } from './Pages/SubServices/subservice.component';
import { StationservicesComponent } from './Pages/StationService/stationservices.component';
import { SubstationserviceComponent } from './Pages/SubStationService/substationservice.component';
import { StationcontactusComponent } from './Pages/StationContactUs/stationcontactus.component';
import { ContactstationstaffComponent } from './Pages/ContactStationStaff/contactstationstaff.component';
import { TeamComponent } from './Pages/Team/team.component';
import { OfcContactComponent } from './Pages/OfcContacts/ofc-contact.component';
import { StationComponent } from './Pages/Stations/station.component';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LeftmenuComponent,
    TopbarComponent,
    LayoutComponent,
    ServiceComponent,
    CategoryComponent,
    GeneralComponent,
    ClientComponent,
    SubcategoryComponent,
    NewsComponent,
    CertificationComponent,
    SubserviceComponent,
    StationservicesComponent,
    SubstationserviceComponent,
    StationcontactusComponent,
    ContactstationstaffComponent,
    TeamComponent,
    OfcContactComponent,
    StationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DataTablesModule,
    NgxMaskModule.forRoot(),
    ChartsModule,
    NgxExtendedPdfViewerModule ,
    MatAutocompleteModule,
    MatIconModule,
    NgxPrintModule,
    PortalModule,
    IgxDatePickerModule,
    MatSelectModule,
    HammerModule,
    CKEditorModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    IgxIconModule,
    IgxInputGroupModule,
    IgxButtonModule,
    IgxRippleModule,
    IgxTimePickerModule,
    TimepickerModule.forRoot(),
    IgxCheckboxModule, IgxGridModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatAutocompleteModule,
    PdfViewerModule,
    AutocompleteLibModule,
    IgxExpansionPanelModule, IgxComboModule, IgxDropDownModule, IgxSelectModule, IgxMaskModule
  ],
  providers: [DatePipe,ApiService, Auth, BnNgIdleService, { provide: LocationStrategy, useClass: HashLocationStrategy }, Pipe, PopoutService],
  bootstrap: [AppComponent]
})
export class AppModule { }
