export class SubcategoryModelRequest {
  catId :string;
  subCatId:string;
  subCategoryName : string;
  disSeq:string;
  IsActive:string;
  catName:string;
}
export class SubcategoryModelResponse {
  catId :string;
  subCatId:string;
  subCategoryName : string;
  disSeq:string;
  catName:string;
  isActive:string;
}
