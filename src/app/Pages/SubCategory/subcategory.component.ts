import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { SubcategoryModelRequest, SubcategoryModelResponse } from './Subcategory.Model';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  subCategoryForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true
  categoryModelResponse:any;
  SubcategoryModelRequest:SubcategoryModelRequest;
  SubcategoryModelResponse:SubcategoryModelResponse[];
// @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;

  constructor(private API: ApiService) {
    this.SubcategoryModelRequest= new SubcategoryModelRequest();
    this.SubcategoryModelResponse=[];
   }

  ngOnInit(): void {
    this. getCategory();
    this.InitializeForm();
    this.getSubCategory();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.subCategoryForm.reset();
      this.subCategoryForm.reset(this.subCategoryForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.subCategoryForm.controls.IsActive.setValue(true);
    } else {
      this.subCategoryForm.controls.IsActive.setValue(false);
    }
  }
  InitializeForm(): any {
    this.subCategoryForm = new FormGroup({
      subCatId:new FormControl(""),
      subCategoryName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      catId:new FormControl("",[Validators.required]),
      IsActive:new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.subCategoryForm.controls; }

  getCategory() {
    this.categoryModelResponse = [];
    this.API.getdata('/Menzies/getCategoryList').subscribe(
      data => {
        if (data != null) {
          this.categoryModelResponse = data.CategoryList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }

  addSubCategory() {

    //this.validations();
    this.submitted = true;
    if (this.subCategoryForm.valid == true) {
      if (this.subCategoryForm.controls.IsActive.value == "" || this.subCategoryForm.controls.IsActive.value == null) {
        this.subCategoryForm.controls.IsActive.setValue(false);
      }
      this.SubcategoryModelRequest = this.subCategoryForm.value;
      this.API.PostData('/Menzies/SaveSubCategory', this.SubcategoryModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Sub Category Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Sub Category Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getSubCategory();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  getSubCategory() {
    this.SubcategoryModelResponse = [];
    this.API.getdata('/Menzies/getSubCategoryList').subscribe(
      data => {
        if (data != null) {

          this.SubcategoryModelResponse = data.SubCategoryList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editSubCategory(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.subCategoryForm.patchValue(p);
    this.subCategoryForm.controls.IsActive.patchValue(p.isActive);
  }
}
