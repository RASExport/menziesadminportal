import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { generalModelRequest, generalModelResponse } from './General.Model';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  generalForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;

  generalModelRequest:generalModelRequest;
  generalModelResponse:generalModelResponse[];

  // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.generalModelRequest= new generalModelRequest();
    this.generalModelResponse=[];
   }

  ngOnInit(): void {
    this.getGeneral();
    this.InitializeForm()
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.generalForm.reset();
      this.generalForm.reset(this.generalForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  InitializeForm(): any {
    this.generalForm = new FormGroup({
      infoID:new FormControl(""),
      Information: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      disSeq:new FormControl("",[Validators.required]),
      infoDetail: new FormControl("",[Validators.required]),
      isActive:new FormControl(""),
    });
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.generalForm.controls; }

  isActiveCheck(check: boolean) {
    if (check == true) {
      this.generalForm.controls.isActive.setValue(true);
    } else {
      this.generalForm.controls.isActive.setValue(false);
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  addGeneral() {
    //this.validations();
    this.submitted = true;
    if (this.generalForm.valid == true) {
      if (this.generalForm.controls.isActive.value == "" || this.generalForm.controls.isActive.value == null) {
        this.generalForm.controls.isActive.setValue(false);
      }
      this.generalModelRequest = this.generalForm.value;
      this.API.PostData('/Menzies/SaveGeneral', this.generalModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'General Info Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'General Info Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getGeneral();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getGeneral() {
    this.generalModelResponse = [];
    this.API.getdata('/Menzies/getGeneralList').subscribe(
      data => {
        if (data != null) {
          this.generalModelResponse = data.GeneralList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editGeneral(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.generalForm.patchValue(p);
  }
  validations() {
    this.submitted = true;
    if (this.generalForm.controls.Information.value == "" || this.generalForm.controls.Information.value == null) {
      Swal.fire({
        text: "Enter General Info Name",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.formValid = false;
      return;
    }
    this.formValid = true;
  }
}
