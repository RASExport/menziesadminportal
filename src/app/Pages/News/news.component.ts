import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { newsModelRequest, newsModelResponse } from './News.Model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  newsForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  newsModelRequest: newsModelRequest;
  newsModelResponse: newsModelResponse[];
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.newsModelRequest = new newsModelRequest();
    this.newsModelResponse = [];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getNews();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.imageUrl ="";
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.newsForm.reset();
      this.imageUrl ="";
      this.newsForm.reset(this.newsForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.newsForm.controls; }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.newsForm.controls.isActive.setValue(true);
    } else {
      this.newsForm.controls.isActive.setValue(false);
    }
  }
  InitializeForm(): any {
    this.newsForm = new FormGroup({
      disSeq: new FormControl("", [Validators.required]),
      newsID: new FormControl(""),
      newsDetails: new FormControl("", [Validators.required, this.noWhitespaceValidator]),
      newsImage: new FormControl(""),
      isActive: new FormControl(""),
    });
  }
  //Attach Image..........................................................................................

  attachImage(file: any) {

    if (!file.target.files)
      return;
    this.fileToUpload = file.target.files[0];
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  addNews() {
    //this.validations();
    this.submitted = true;
    if (this.newsForm.valid == true) {
      if (this.newsForm.controls.isActive.value == "" || this.newsForm.controls.isActive.value == null) {
        this.newsForm.controls.isActive.setValue(false);
      }
      this.newsModelRequest = this.newsForm.value;
      if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
        this.imageUrl = ""
        Swal.fire({
          text: 'Attach Image',
          icon: 'warning',
          confirmButtonText: 'OK'
        });
        return
      }
      else {
        this.newsModelRequest.newsImage = this.imageUrl;
      }
      this.API.PostData('/Menzies/SaveNews', this.newsModelRequest).subscribe(
        c => {
          if (c != null) {
            if (c.Status == "Failed") {
              Swal.fire({
                text: 'News Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else {
              Swal.fire({
                text: 'News Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getNews();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getNews() {

    this.newsModelResponse = [];
    this.API.getdata('/Menzies/getNewsList').subscribe(
      data => {
        if (data != null) {
          this.newsModelResponse = data.NewsList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editNews(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.newsForm.patchValue(p);
    this.imageUrl = p.newsImage
  }
}
