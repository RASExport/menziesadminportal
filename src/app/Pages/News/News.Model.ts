export class newsModelRequest {
  newsID :string;
  newsDetails : string;
  newsImage:string;
  isActive:string;
}
export class newsModelResponse {
  newsID :string;
  newsDetails : string;
  newsImage:string;
  isActive:string;
  disSeq:string;
}
