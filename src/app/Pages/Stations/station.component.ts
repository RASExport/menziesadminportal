import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { stationModelRequest, stationModelResponse } from './station.Model';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  stationForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  stationModelRequest:stationModelRequest;
  stationModelResponse:stationModelResponse[];
  // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.stationModelRequest= new stationModelRequest();
    this.stationModelResponse=[];
   }

  ngOnInit(): void {
    this.InitializeForm();
    this.getStation();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.stationForm.reset();
      this.stationForm.reset(this.stationForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.stationForm.controls.isActive.setValue(true);
    } else {
      this.stationForm.controls.isActive.setValue(false);
    }
  }
  InitializeForm(): any {
    this.stationForm = new FormGroup({
      stationId:new FormControl(""),
      stationName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      isActive:new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.stationForm.controls; }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  addStations() {
    //this.validations();

    this.submitted = true;
    if (this.stationForm.valid == true) {
      if (this.stationForm.controls.isActive.value == "" || this.stationForm.controls.isActive.value == null) {
        this.stationForm.controls.isActive.setValue(false);
      }
      this.stationModelRequest = this.stationForm.value;
      this.API.PostData('/Menzies/SaveStation', this.stationModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Station Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Station Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getStation();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getStation() {
    this.stationModelResponse = [];
    this.API.getdata('/Menzies/getStationList').subscribe(
      data => {
        if (data != null) {
          this.stationModelResponse = data.stationList;
        }
        else {
        }
      },
      error => {
        // Swal.fire({
        //   text: error.error.Message,
        //   icon: 'error',
        //   confirmButtonText: 'OK'
        // });
      });
  }
editstation(p){
  window.scroll(0,0);
  this.showhide('Edit');
  this.stationForm.patchValue(p);
}
}
