export class stationModelRequest {
  stationId:string;
  stationName:string;
  isActive:string;
  disSeq:string;
}
export class stationModelResponse {
  stationId:string;
  stationName:string;
  isActive:string;
  disSeq:string;
}
