import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubstationserviceComponent } from './substationservice.component';

describe('SubstationserviceComponent', () => {
  let component: SubstationserviceComponent;
  let fixture: ComponentFixture<SubstationserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubstationserviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubstationserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
