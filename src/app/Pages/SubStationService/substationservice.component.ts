import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { stationModelResponse, SubStationModelRequest, SubStationModelResponse } from './SubStationService.Model';

@Component({
  selector: 'app-substationservice',
  templateUrl: './substationservice.component.html',
  styleUrls: ['./substationservice.component.css']
})
export class SubstationserviceComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  subStationServiceForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  SubStationModelRequest:SubStationModelRequest;
  SubStationModelResponse:SubStationModelResponse[];
  stationModelResponse:stationModelResponse[];
  // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.SubStationModelRequest= new SubStationModelRequest();
    this.SubStationModelResponse=[];
    this.stationModelResponse=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getSubStation();
    this.getStation();
  }
  InitializeForm(): any {
    this.subStationServiceForm = new FormGroup({
      subStationServiceId:new FormControl(""),
      subStationServiceName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      isActive:new FormControl(""),
      stationId:new FormControl(""),
      subStationServiceDescription:new FormControl("")
    });
  }

  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.subStationServiceForm.reset();
      this.subStationServiceForm.reset(this.subStationServiceForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.subStationServiceForm.controls.isActive.setValue(true);
    } else {
      this.subStationServiceForm.controls.isActive.setValue(false);
    }
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.subStationServiceForm.controls; }
  addSubStation() {

    //this.validations();
    this.submitted = true;
    if (this.subStationServiceForm.valid == true) {
      if (this.subStationServiceForm.controls.isActive.value == "" || this.subStationServiceForm.controls.isActive.value == null) {
        this.subStationServiceForm.controls.isActive.setValue(false);
      }
      this.SubStationModelRequest = this.subStationServiceForm.value;
      this.API.PostData('/Menzies/SaveSubStationService', this.SubStationModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'SubStationService Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'SubStationService Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getSubStation();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  editSubStation(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.subStationServiceForm.patchValue(p);
  }
  getSubStation() {
    this.SubStationModelResponse = [];
    this.API.getdata('/Menzies/getSubStationServiceList').subscribe(
      data => {
        if (data != null) {
          this.SubStationModelResponse = data.getsubservicestationwise;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  getStation() {
    this.stationModelResponse = [];
    this.API.getdata('/Menzies/getStationList_Web').subscribe(
      data => {
        if (data != null) {
          this.stationModelResponse = data.stationList;
        }
        else {
        }
      },
      error => {
        // Swal.fire({
        //   text: error.error.Message,
        //   icon: 'error',
        //   confirmButtonText: 'OK'
        // });
      });
  }
}
