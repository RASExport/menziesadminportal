export class SubStationModelRequest {
  subStationServiceId :string;
  subStationServiceName :string;
  subStationServiceDescription :string;
  isActive :string;
  stationId:any;
  stationName:any;
}
export class SubStationModelResponse {
  subStationServiceId :string;
  subStationServiceName :string;
  subStationServiceDescription :string;
  isActive :string;
  stationId:any;
  stationName:any;
 }
 export class stationModelResponse {
  stationId:any;
  stationName:any;
  isActive:any;
}
