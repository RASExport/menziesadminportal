import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { subServiceModelRequest, subServiceModelResponse } from './SubService.Model';

@Component({
  selector: 'app-subservice',
  templateUrl: './subservice.component.html',
  styleUrls: ['./subservice.component.css']
})
export class SubserviceComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  subServicesForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true
  serviceModelResponse: any;
  subServiceModelRequest: subServiceModelRequest;
  subServiceModelResponse: subServiceModelResponse[];
  constructor(private API: ApiService) {
    this.subServiceModelRequest = new subServiceModelRequest();
    this.subServiceModelResponse = [];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getService();
    this.getSubService();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.imageUrl = "";
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.imageUrl = "";
      this.subServicesForm.reset();
      this.subServicesForm.reset(this.subServicesForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  InitializeForm(): any {
    this.subServicesForm = new FormGroup({
      subServiceId: new FormControl(""),
      serviceID: new FormControl(""),
      subServiceName: new FormControl("",[Validators.required, this.noWhitespaceValidator,Validators.pattern('^[a-zA-Z \-\']+')]),
      subServiceDetails: new FormControl(""),
      subServiceImage: new FormControl(""),
      disSeq: new FormControl(""),
      isActive: new FormControl(""),
    });
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.subServicesForm.controls; }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.subServicesForm.controls.isActive.setValue(true);
    } else {
      this.subServicesForm.controls.isActive.setValue(false);
    }
  }

  //Attach Image..........................................................................................

  attachImage(file: any) {
    if (!file.target.files)
      return;
    this.fileToUpload = file.target.files[0];
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  getService() {

    this.serviceModelResponse = [];
    this.API.getdata('/Menzies/getServiceList').subscribe(
      data => {
        if (data != null) {
          this.serviceModelResponse = data.ServiceList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }

  addSubService() {
    //this.validations();
    this.submitted = true;
    if (this.subServicesForm.valid == true) {
      if (this.subServicesForm.controls.isActive.value == "" || this.subServicesForm.controls.isActive.value == null) {
        this.subServicesForm.controls.isActive.setValue(false);
      }

      this.subServiceModelRequest = this.subServicesForm.value;

      if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
        this.imageUrl = ""
        Swal.fire({
          text: 'Attach Image',
          icon: 'warning',
          confirmButtonText: 'OK'
        });
        return
      }
      else {
        this.subServiceModelRequest.subServiceImage = this.imageUrl;
      }
      this.API.PostData('/Menzies/SaveSubService', this.subServiceModelRequest).subscribe(
        c => {
          if (c != null) {
            if (c.Status == "Failed") {
              Swal.fire({
                text: 'SubService Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else {
              Swal.fire({
                text: 'SubService Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getSubService();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getSubService() {
    this.subServiceModelResponse = [];
    this.API.getdata('/Menzies/getSubServiceList').subscribe(
      data => {
        if (data != null) {
          this.subServiceModelResponse = data.ServiceList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editsubCat(p) {
    window.scroll(0,0);
    this.showhide('Edit');
    this.subServicesForm.patchValue(p);
    this.imageUrl = p.subServiceImage;
  }
}
