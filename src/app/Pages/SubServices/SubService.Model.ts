export class subServiceModelRequest {
  subServiceId: string;
  serviceID: string;
  subServiceName: string;
  subServiceDetails: string;
  subServiceImage: string;
  disSeq: string;
  isActive:string;
}
export class subServiceModelResponse {
  serviceID: string;
  serviceName: string;
  subServiceId: string;
  subServiceName: string;
  subServiceDetails: string;
  subServiceImage: string;
  disSeq: string;
  isActive: string;
}
