import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfcContactComponent } from './ofc-contact.component';

describe('OfcContactComponent', () => {
  let component: OfcContactComponent;
  let fixture: ComponentFixture<OfcContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfcContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfcContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
