import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { MapResponse, ofcContactModelRequest, ofcContactModelResponse } from './OfcContact.Mode';

@Component({
  selector: 'app-ofc-contact',
  templateUrl: './ofc-contact.component.html',
  styleUrls: ['./ofc-contact.component.css']
})
export class OfcContactComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  ofcContactForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  MapResponse:MapResponse[];
  ofcContactModelRequest:ofcContactModelRequest;
  ofcContactModelResponse:ofcContactModelResponse[];
  // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.ofcContactModelRequest= new ofcContactModelRequest();
    this.ofcContactModelResponse=[];
    this.MapResponse=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getOfcContact();
    this.getMap();
  }

  InitializeForm(): any {
    this.ofcContactForm = new FormGroup({
      ofcContactId:new FormControl(""),
      ofcName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      ofcAddress:new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      ofcHeadName:new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      ofcHeadDesignation:new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      ofcHeadEmail:new FormControl("", [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      ofcHeadPhone:new FormControl("",[Validators.required, Validators.pattern(".{10,10}")]),
      isActive:new FormControl(""),
      mapId:new FormControl(""),
      disSeq:new FormControl("",[Validators.required]),

    });
  }

  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.ofcContactForm.reset();
      this.ofcContactForm.reset(this.ofcContactForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.ofcContactForm.controls.isActive.setValue(true);
    } else {
      this.ofcContactForm.controls.isActive.setValue(false);
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.ofcContactForm.controls; }

  addOfcContacts() {
    //this.validations();

    this.submitted = true;
    if (this.ofcContactForm.valid == true) {
      if (this.ofcContactForm.controls.isActive.value == "" || this.ofcContactForm.controls.isActive.value == null) {
        this.ofcContactForm.controls.isActive.setValue(false);
      }
      this.ofcContactModelRequest = this.ofcContactForm.value;
      this.API.PostData('/Menzies/SaveOfcContact', this.ofcContactModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Contact Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Contact Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getOfcContact();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }

  getOfcContact() {

    this.ofcContactModelResponse = [];
    this.API.getdata('/Menzies/getOfcContact').subscribe(
      data => {
        if (data != null) {
          this.ofcContactModelResponse = data.OfcContactList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editOfcContact(p){
    window.scroll(0,0);
    this.showhide('Edit');
    this.ofcContactForm.patchValue(p);
  }

  getMap() {

    this.MapResponse = [];
    this.API.getdata('/Menzies/getMapList').subscribe(
      data => {
        if (data != null) {
          this.MapResponse = data.getMapList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
}
