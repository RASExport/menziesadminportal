export class ofcContactModelRequest {
  ofcContactId:string;
  ofcName:string;
  ofcAddress:string;
  ofcHeadName:string;
  ofcHeadDesignation:string;
  ofcHeadEmail:string;
  ofcHeadPhone:string;
  disSeq:string;
  isActive:string;
  mapId:string;
}
export class ofcContactModelResponse {
  ofcContactId:string;
  ofcName:string;
  ofcAddress:string;
  ofcHeadName:string;
  ofcHeadDesignation:string;
  ofcHeadEmail:string;
  ofcHeadPhone:string;
  disSeq:string;
  isActive:string;
  mapId:string;
}
export class MapResponse{
  mapId:string;
  mapName:string;
  mapSource:string;
}
