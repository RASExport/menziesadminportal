import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { stationModelResponse, stationServiceModelRequest, stationServiceModelResponse } from './StationService.Model';

@Component({
  selector: 'app-stationservices',
  templateUrl: './stationservices.component.html',
  styleUrls: ['./stationservices.component.css']
})
export class StationservicesComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  serviceStationForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  stationServiceModelRequest:stationServiceModelRequest;
  stationServiceModelResponse:stationServiceModelResponse[];
  stationModelResponse:stationModelResponse[];
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.stationServiceModelRequest= new stationServiceModelRequest();
    this.stationServiceModelResponse=[];
    this.stationModelResponse=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getStationService();
    this.getStation();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.imageUrl = "";
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.imageUrl = "";
      this.serviceStationForm.reset();
      this.serviceStationForm.reset(this.serviceStationForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }

  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.serviceStationForm.controls; }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.serviceStationForm.controls.isActive.setValue(true);
    } else {
      this.serviceStationForm.controls.isActive.setValue(false);
    }
  }
  //Attach Image..........................................................................................

attachImage(file: any) {

  if (!file.target.files)
    return;
  this.fileToUpload = file.target.files[0];
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
  }
  reader.readAsDataURL(this.fileToUpload);
}
InitializeForm(): any {
  this.serviceStationForm = new FormGroup({
    stationServiceId:new FormControl(""),
    stationDescription: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
    stationServiceImage: new FormControl(""),
    stationId: new FormControl(""),
    isActive:new FormControl(""),
    stationServiceName:new FormControl("",[Validators.required, this.noWhitespaceValidator]),
  });
}
addCServiceStation() {

  //this.validations();
  this.submitted = true;
  if (this.serviceStationForm.valid == true) {
    if (this.serviceStationForm.controls.isActive.value == "" || this.serviceStationForm.controls.isActive.value == null) {
      this.serviceStationForm.controls.isActive.setValue(false);
    }
    this.stationServiceModelRequest = this.serviceStationForm.value;
   if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
    this.imageUrl = ""
    Swal.fire({
      text: 'Attach Image',
      icon: 'warning',
      confirmButtonText: 'OK'
    });
    return
  }
   else {
     this.stationServiceModelRequest.stationServiceImage = this.imageUrl;
   }
    this.API.PostData('/Menzies/SaveStationService',this.serviceStationForm.value).subscribe(
      c => {
        if (c != null) {
          if(c.Status=="Failed"){
            Swal.fire({
              text: 'Service Station Already Exist.',
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
          else{
            Swal.fire({
              text: 'Service Station Saved Successfully',
              icon: 'success',
              confirmButtonText: 'OK'
            });
            this.getStationService();
            this.showhide("Cancel");
          }

        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
}
editServiceStation(p) {
  this.showhide("Edit");
  this.serviceStationForm.patchValue(p);
  this.imageUrl = p.stationServiceImage
}

getStationService() {
  this.stationServiceModelResponse = [];
  this.API.getdata('/Menzies/getStationServiceList').subscribe(
    data => {
      if (data != null) {
        this.stationServiceModelResponse = data.getStationWiseList;
      }
      else {
      }
    },
    error => {
      Swal.fire({
        text: error.error.Message,
        icon: 'error',
        confirmButtonText: 'OK'
      });
    });
}
getStation() {
  this.stationModelResponse = [];
  this.API.getdata('/Menzies/getStationList').subscribe(
    data => {
      if (data != null) {
        this.stationModelResponse = data.stationList;
      }
      else {
      }
    },
    error => {
      // Swal.fire({
      //   text: error.error.Message,
      //   icon: 'error',
      //   confirmButtonText: 'OK'
      // });
    });
}
}
