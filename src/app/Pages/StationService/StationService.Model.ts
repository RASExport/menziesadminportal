export class stationServiceModelRequest {
  stationServiceId:string;
  stationName:string;
  stationDescription:string;
  stationServiceImage:string;
  isActive:string;
  stationId:string;
}
export class stationServiceModelResponse {
  stationServiceId:string;
  stationName:string;
  stationDescription:string;
  stationServiceImage:string;
  isActive:string;
  stationId:string;
  stationServiceName:string;
}
export class stationModelResponse {
  stationId:any;
  stationName:any;
  isActive:any;
}


