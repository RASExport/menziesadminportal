import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StationservicesComponent } from './stationservices.component';

describe('StationservicesComponent', () => {
  let component: StationservicesComponent;
  let fixture: ComponentFixture<StationservicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StationservicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StationservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
