import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { categoryModelRequest, categoryModelResponse } from './Category.Model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  categoryForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  categoryModelRequest:categoryModelRequest;
  categoryModelResponse:categoryModelResponse[];
// @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;

  constructor(private API: ApiService) {
    this.categoryModelRequest= new categoryModelRequest();
    this.categoryModelResponse=[];
   }

  ngOnInit(): void {
    this.InitializeForm();
    this.getCategory();
  }

  InitializeForm(): any {
    this.categoryForm = new FormGroup({
      catId:new FormControl(""),
      catName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      isActive:new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.categoryForm.reset();
      this.categoryForm.reset(this.categoryForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.categoryForm.controls.isActive.setValue(true);
    } else {
      this.categoryForm.controls.isActive.setValue(false);
    }
  }

  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.categoryForm.controls; }

  addCategory() {
    //this.validations();

    this.submitted = true;
    if (this.categoryForm.valid == true) {
      if (this.categoryForm.controls.isActive.value == "" || this.categoryForm.controls.isActive.value == null) {
        this.categoryForm.controls.isActive.setValue(false);
      }
      this.categoryModelRequest = this.categoryForm.value;
      this.API.PostData('/Menzies/SaveCategory', this.categoryModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Category Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Category Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getCategory();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  editCategory(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.categoryForm.patchValue(p);
  }
  getCategory() {

    this.categoryModelResponse = [];
    this.API.getdata('/Menzies/getCategoryList').subscribe(
      data => {
        if (data != null) {
          this.categoryModelResponse = data.CategoryList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  validations() {
    this.submitted = true;
    if (this.categoryForm.controls.catName.value == "" || this.categoryForm.controls.catName.value == null ) {
      Swal.fire({
        text: "Enter Category Name",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.formValid = false;
      return;
    }
    this.formValid = true;
  }

}
