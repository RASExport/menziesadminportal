export class categoryModelRequest {
  catId :string;
  catName : string;
  disSeq:string;
  isActive:string;
}
export class categoryModelResponse {
  catId :string;
  catName : string;
  disSeq:string;
  isActive:string;
}

