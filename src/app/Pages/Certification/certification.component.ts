import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { certificateModelRequest, certificateModelResponse } from './Certificate.Model';

@Component({
  selector: 'app-certification',
  templateUrl: './certification.component.html',
  styleUrls: ['./certification.component.css']
})
export class CertificationComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  certificateForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  certificateModelResponse: certificateModelResponse[];
  certificateModelRequest: certificateModelRequest;

  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.certificateModelRequest = new certificateModelRequest();
    this.certificateModelResponse = [];
  }

  ngOnInit(): void {
    this.getCertificate();
    this.InitializeForm();
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.certificateForm.controls.isActive.setValue(true);
    } else {
      this.certificateForm.controls.isActive.setValue(false);
    }
  }
  InitializeForm(): any {
    this.certificateForm = new FormGroup({
      certificateID: new FormControl(""),
      certificateName: new FormControl("", [Validators.required, this.noWhitespaceValidator]),
      certificateDescription: new FormControl(""),
      certificateLogo: new FormControl(""),
      isActive: new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.certificateForm.controls; }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.imageUrl = "";
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.imageUrl = "";
      this.certificateForm.reset();
      this.certificateForm.reset(this.certificateForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }

  attachImage(file: any) {

    if (!file.target.files)
      return;
    this.fileToUpload = file.target.files[0];
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  addCertificate() {
    //this.validations();
    this.submitted = true;
    if (this.certificateForm.valid == true) {
      if (this.certificateForm.controls.isActive.value == "" || this.certificateForm.controls.isActive.value == null) {
        this.certificateForm.controls.isActive.setValue(false);
      }
      this.certificateModelRequest = this.certificateForm.value;
      if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
        this.imageUrl = ""
        Swal.fire({
          text: 'Attach Logo',
          icon: 'warning',
          confirmButtonText: 'OK'
        });
        return
      }
      else {
        this.certificateModelRequest.certificateLogo = this.imageUrl;
      }
      this.API.PostData('/Menzies/SaveCertificate', this.certificateModelRequest).subscribe(
        c => {
          if (c != null) {
            if (c.Status == "Failed") {
              Swal.fire({
                text: 'Certificate Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else {
              Swal.fire({
                text: 'Certificate Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getCertificate();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  editCertificate(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.certificateForm.patchValue(p);
    this.imageUrl = p.certificateLogo
  }
  getCertificate() {

    this.certificateModelResponse = [];
    this.API.getdata('/Menzies/getCertificateList').subscribe(
      data => {
        if (data != null) {
          this.certificateModelResponse = data.CertificateList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
}
