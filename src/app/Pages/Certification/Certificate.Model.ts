export class certificateModelRequest {
  certificateID :string;
  certificateName : string;
  certificateDescription:string;
  certificateLogo:string;
  isActive:string;
  disSeq:string;
}
export class certificateModelResponse {
  certificateID :string;
  certificateName : string;
  certificateDescription:string;
  certificateLogo:string;
  isActive:string;
  disSeq:string;
}
