import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { stationModelResponse } from '../StationContactUs/StationContactus.Model';
import { contactStationStaffModelRequest, contactStationStaffModelResponse } from './ContactStationStaff.Model';

@Component({
  selector: 'app-contactstationstaff',
  templateUrl: './contactstationstaff.component.html',
  styleUrls: ['./contactstationstaff.component.css']
})
export class ContactstationstaffComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  contactStationForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  stationServiceModelResponse:any;
  stationModelResponse:stationModelResponse[];
  contactStationStaffModelRequest:contactStationStaffModelRequest;
  contactStationStaffModelResponse:contactStationStaffModelResponse[];
   // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.contactStationStaffModelRequest= new contactStationStaffModelRequest();
    this.contactStationStaffModelResponse=[];
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.contactStationForm.reset();
      this.contactStationForm.reset(this.contactStationForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.contactStationForm.controls.isActive.setValue(true);
    } else {
      this.contactStationForm.controls.isActive.setValue(false);
    }
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.contactStationForm.controls; }
  ngOnInit(): void {
    this.getStation();
    this.InitializeForm();
    this.getContactStationStaff();
  }
  InitializeForm(): any {
    this.contactStationForm = new FormGroup({
      contactStationStaffId:new FormControl(""),
      staffDesignation: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      isActive: new FormControl(""),
      stationId: new FormControl(""),
    });
  }

  getStation() {
    this.stationModelResponse = [];
    this.API.getdata('/Menzies/getStationList_Web').subscribe(
      data => {
        if (data != null) {
          this.stationModelResponse = data.stationList;
        }
        else {
        }
      },
      error => {
        // Swal.fire({
        //   text: error.error.Message,
        //   icon: 'error',
        //   confirmButtonText: 'OK'
        // });
      });
  }
  addStationContactStaff() {
    //this.validations();
    this.submitted = true;
    if (this.contactStationForm.valid == true) {
      if (this.contactStationForm.controls.isActive.value == "" || this.contactStationForm.controls.isActive.value == null) {
        this.contactStationForm.controls.isActive.setValue(false);
      }
      this.contactStationStaffModelRequest = this.contactStationForm.value;
      this.API.PostData('/Menzies/SaveStationContactStaff', this.contactStationStaffModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Contact Station Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Contact Station Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.getContactStationStaff();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getContactStationStaff() {
    this.contactStationStaffModelResponse = [];
    this.API.getdata('/Menzies/getStationContactStaff').subscribe(
      data => {
        if (data != null) {
          this.contactStationStaffModelResponse = data.getcontactstationstaff;
        }
        else {
        }
      },
      error => {
        // Swal.fire({
        //   text: error.error.Message,
        //   icon: 'error',
        //   confirmButtonText: 'OK'
        // });
      });
  }

  editStationContactStaff(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.contactStationForm.patchValue(p);
  }
}
