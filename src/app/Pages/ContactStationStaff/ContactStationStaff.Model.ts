export class contactStationStaffModelRequest {
 contactStationStaffId :string;
 staffDesignation :string;
 isActive :string;
 stationId :string;
 stationName:string;
}
export class contactStationStaffModelResponse {
  contactStationStaffId :string;
  staffDesignation :string;
  isActive :string;
  stationId :string;
  stationName:string;
 }
