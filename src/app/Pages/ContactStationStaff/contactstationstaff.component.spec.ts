import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactstationstaffComponent } from './contactstationstaff.component';

describe('ContactstationstaffComponent', () => {
  let component: ContactstationstaffComponent;
  let fixture: ComponentFixture<ContactstationstaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactstationstaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactstationstaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
