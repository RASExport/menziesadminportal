import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { serviceModelRequest, serviceModelResponse } from './Service.Model';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  fileToUpload: any = null;
  imageUrl: string;
  submitted: boolean = false;
  serviceModelRequest:serviceModelRequest;
  serviceModelResponse:serviceModelResponse[];
  serviceForm: FormGroup;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;

  // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;

  constructor(private API: ApiService) {
    this.serviceModelRequest= new serviceModelRequest();
    this.serviceModelResponse= [];
   }

  ngOnInit(): void {
    this.InitializeForm();
    this.getService();
  }

  InitializeForm(): any {
    this.serviceForm = new FormGroup({
      serviceID:new FormControl(""),
      serviceName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      serviceDescription: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      disSeq:new FormControl("",[Validators.required]),
      isActive:new FormControl(""),
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.serviceForm.controls; }

  showhide(callfrm: string) {

    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.imageUrl = "";
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.serviceForm.reset();
      this.imageUrl = "";
      this.serviceForm.reset(this.serviceForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }

  addService() {
    //this.validations();
    this.submitted = true;
    if (this.serviceForm.valid == true) {
      if (this.serviceForm.controls.isActive.value == "" || this.serviceForm.controls.isActive.value == null) {
        this.serviceForm.controls.isActive.setValue(false);
      }
      this.serviceModelRequest = this.serviceForm.value;
      if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
        this.imageUrl = ""
        Swal.fire({
          text: 'Attach Image',
          icon: 'warning',
          confirmButtonText: 'OK'
        });
        return
      }
      else {
        this.serviceModelRequest.serviceImage = this.imageUrl;
      }
      this.API.PostData('/Menzies/SaveService', this.serviceModelRequest).subscribe(
        c => {
          if (c != null) {
            if(c.Status=="Failed"){
              Swal.fire({
                text: 'Service Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else{
              Swal.fire({
                text: 'Service Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this. getService();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }
  getService() {
    this.serviceModelResponse = [];
    this.API.getdata('/Menzies/getServiceList').subscribe(
      data => {
        if (data != null) {
          this.serviceModelResponse = data.ServiceList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editService(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.serviceForm.patchValue(p);
    this.imageUrl = p.serviceImage
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.serviceForm.controls.isActive.setValue(true);
    } else {
      this.serviceForm.controls.isActive.setValue(false);
    }
  }

  validations() {
    this.submitted = true;
    if (this.serviceForm.controls.serviceName.value == "" || this.serviceForm.controls.serviceName.value == null) {
      Swal.fire({
        text: "Enter Service Name",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.formValid = false;
      return;
    }
    this.formValid = true;
  }
  //Attach Image..........................................................................................

attachImage(file: any) {

  if (!file.target.files)
    return;
  this.fileToUpload = file.target.files[0];
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
  }
  reader.readAsDataURL(this.fileToUpload);
}
}
