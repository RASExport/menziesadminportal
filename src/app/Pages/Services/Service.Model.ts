export class serviceModelRequest {
  serviceID :string;
  serviceName : string;
  serviceDescription : string;
  serviceImage:string;
  disSeq:string;
  isActive:string;
}
export class serviceModelResponse {
  serviceID :string;
  serviceName : string;
  serviceDescription : string;
  serviceImage:string;
  disSeq:string;
  isActive:boolean;
}
