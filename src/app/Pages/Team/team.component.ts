import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { teamModelRequest, teamModelResponse } from './team.Model';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  teamForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;

  teamModelRequest:teamModelRequest;
  teamModelResponse:teamModelResponse[];
  constructor(private API: ApiService) {
    this.teamModelRequest= new teamModelRequest();
    this.teamModelResponse=[];
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getTeam();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
      this.imageUrl ="";
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.teamForm.reset();
      this.imageUrl ="";
      this.teamForm.reset(this.teamForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.teamForm.controls.isActive.setValue(true);
    } else {
      this.teamForm.controls.isActive.setValue(false);
    }
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.teamForm.controls; }

  InitializeForm(): any {
    this.teamForm = new FormGroup({
      teamId:new FormControl(""),
      teamName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      isActive:new FormControl(""),
      teamDesignation:new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      teamDescription:new FormControl(""),
      teamImage:new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
 //Attach Image..........................................................................................

 attachImage(file: any) {

  if (!file.target.files)
    return;
  this.fileToUpload = file.target.files[0];
  //Show image preview
  var reader = new FileReader();
  reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
  }
  reader.readAsDataURL(this.fileToUpload);
}
addTeam() {
  //this.validations();
  this.submitted = true;
  if (this.teamForm.valid == true) {
    if (this.teamForm.controls.isActive.value == "" || this.teamForm.controls.isActive.value == null) {
      this.teamForm.controls.isActive.setValue(false);
    }
    this.teamModelRequest = this.teamForm.value;
    if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
      this.imageUrl = ""
      Swal.fire({
        text: 'Attach Image',
        icon: 'warning',
        confirmButtonText: 'OK'
      });
      return
    }
    else {
      this.teamModelRequest.teamImage = this.imageUrl;
    }
    this.API.PostData('/Menzies/SaveTeam', this.teamModelRequest).subscribe(
      c => {
        if (c != null) {
          if (c.Status == "Failed") {
            Swal.fire({
              text: 'SubService Already Exist.',
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
          else {
            Swal.fire({
              text: 'SubService Saved Successfully',
              icon: 'success',
              confirmButtonText: 'OK'
            });
            this.getTeam();
            this.showhide("Cancel");
          }

        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
}
getTeam() {

  this.teamModelResponse = [];
  this.API.getdata('/Menzies/getTeam').subscribe(
    data => {
      if (data != null) {
        this.teamModelResponse = data.TeamList;
      }
      else {
      }
    },
    error => {
      Swal.fire({
        text: error.error.Message,
        icon: 'error',
        confirmButtonText: 'OK'
      });
    });
}
editTeam(p){
  window.scroll(0,0);
  this.showhide('Edit');
  this.teamForm.patchValue(p);
  this.imageUrl = p.teamImage;
}
}
