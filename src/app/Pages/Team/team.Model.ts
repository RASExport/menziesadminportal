export class teamModelRequest {
  teamId:string;
  teamName:string;
  teamDesignation:string;
  teamDescription:string;
  isActive:string;
  disSeq:string;
  teamImage:string;
}
export class teamModelResponse {
  teamId:string;
  teamName:string;
  teamDesignation:string;
  teamDescription:string;
  isActive:string;
  disSeq:string;
  teamImage:string;
}

