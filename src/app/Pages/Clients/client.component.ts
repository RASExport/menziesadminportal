import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { clientModelRequest, clientModelResponse, corporativeModelResponse } from './Client.Model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  imageUrl: string;
  fileToUpload: any = null;
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  clientForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  clientModelRequest: clientModelRequest;
  clientModelResponse: clientModelResponse[];
  corporativeModelResponse: corporativeModelResponse[];
  @ViewChildren(DataTableDirective)
  datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  constructor(private API: ApiService) {
    this.clientModelRequest = new clientModelRequest();
    this.clientModelResponse = [];
    this.corporativeModelResponse = [];
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.imageUrl = "";
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.imageUrl = "";
      this.clientForm.reset();
      this.clientForm.reset(this.clientForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  InitializeForm(): any {
    this.clientForm = new FormGroup({
      clientID: new FormControl(""),
      clientName: new FormControl("", [Validators.required, this.noWhitespaceValidator]),
      clientDescription: new FormControl(""),
      clientLogo: new FormControl(""),
      isCorporate: new FormControl(""),
      isActive: new FormControl(""),
      disSeq:new FormControl("",[Validators.required])
    });
  }
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.clientForm.controls; }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.clientForm.controls.isActive.setValue(true);
    } else {
      this.clientForm.controls.isActive.setValue(false);
    }
  }

  isCorporateCheck(check: boolean) {
    if (check == true) {
      this.clientForm.controls.isCorporate.setValue(true);
    } else {
      this.clientForm.controls.isCorporate.setValue(false);
    }
  }

  //Attach Image..........................................................................................

  attachImage(file: any) {
    if (!file.target.files)
      return;
    this.fileToUpload = file.target.files[0];
    //Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  ngOnInit(): void {
    this.InitializeForm();
    this.getClient();
  }

  addClients() {
    //this.validations();
    this.submitted = true;
    if (this.clientForm.valid == true) {
      if (this.clientForm.controls.isActive.value == "" || this.clientForm.controls.isActive.value == null) {
        this.clientForm.controls.isActive.setValue(false);
      }
      this.clientModelRequest = this.clientForm.value;
      if (this.imageUrl == null || this.imageUrl == "" || this.imageUrl == undefined) {
        this.imageUrl = ""
        Swal.fire({
          text: 'Attach Logo',
          icon: 'warning',
          confirmButtonText: 'OK'
        });
        return
      }
      else {
        this.clientModelRequest.clientLogo = this.imageUrl;
      }

      this.API.PostData('/Menzies/SaveClient', this.clientModelRequest).subscribe(
        c => {
          if (c != null) {
            if (c.Status == "Failed") {
              Swal.fire({
                text: 'Clients Already Exist.',
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
            else {
              Swal.fire({
                text: 'Clients Saved Successfully',
                icon: 'success',
                confirmButtonText: 'OK'
              });
              this.clientForm.reset();
              this.imageUrl = "";
              this.getClient();
              this.showhide("Cancel");
            }

          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }
  }

  getClient() {
    this.clientModelResponse = [];
    this.API.getdata('/Menzies/getClientList').subscribe(
      data => {
        if (data != null) {
          this.clientModelResponse = data.ClientList;
        }
        else {
        }
      },
      error => {
        Swal.fire({
          text: error.error.Message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      });
  }
  editClient(p) {
    window.scroll(0,0);
    this.showhide("Edit");
    this.clientForm.patchValue(p);
    this.imageUrl = p.clientLogo
  }
  validations() {
    this.submitted = true;
    if (this.clientForm.controls.clientName.value == "" || this.clientForm.controls.clientName.value == null) {
      Swal.fire({
        text: "Enter Client Name",
        icon: 'error',
        confirmButtonText: 'OK'
      });
      this.formValid = false;
      return;
    }
    this.formValid = true;
  }
}
