export class clientModelRequest {
  clientID :string;
  clientName : string;
  clientDescription:string;
  clientLogo:string;
  isCorporate:string;
  isActive:string;
  disSeq:string;
}
export class clientModelResponse {
  clientID :string;
  clientName : string;
  clientDescription:string;
  clientLogo:string;
  isCorporate:string;
  isActive:string;
  disSeq:string;
}
export class corporativeModelResponse {
  clientID :string;
  clientName : string;
  clientDescription:string;
  clientLogo:string;
  isCorporate:string;
  isActive:string;
  disSeq:string;
}

