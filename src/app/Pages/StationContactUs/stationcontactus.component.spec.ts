import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StationcontactusComponent } from './stationcontactus.component';

describe('StationcontactusComponent', () => {
  let component: StationcontactusComponent;
  let fixture: ComponentFixture<StationcontactusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StationcontactusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StationcontactusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
