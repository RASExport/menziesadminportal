
export class stationcontactModelRequest {
  contactId:string;
  contactName:string;
  stationServiceId:string;
  contactDesignation:string;
  contactAddress:string;
  stationName:string;
  isActive:string;
  contactPhone:string;
  contactLocation:string;
  stationId:string;
}

export class stationcontactModelResponse {
  contactId:string;
  contactName:string;
  stationServiceId:string;
  contactDesignation:string;
  contactAddress:string;
  stationName:string;
  isActive:string;
  contactPhone:string;
  contactLocation:string;
  stationId:string;
}
export class stationModelResponse {
  stationId:any;
  stationName:any;
  isActive:any;
}
