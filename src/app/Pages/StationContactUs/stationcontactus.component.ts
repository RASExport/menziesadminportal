import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';
import { stationcontactModelRequest, stationcontactModelResponse, stationModelResponse } from './StationContactus.Model';

@Component({
  selector: 'app-stationcontactus',
  templateUrl: './stationcontactus.component.html',
  styleUrls: ['./stationcontactus.component.css']
})
export class StationcontactusComponent implements OnInit {
  submitted: boolean = false;
  formValid: boolean = false;
  addnewSurvey: boolean = false;
  showSurvey: boolean = true;
  stationContactForm: FormGroup;
  showCancelButton: boolean = false;
  showSaveButton: boolean = false;
  showeditButton: boolean = false;
  shownewButton: boolean = true;
  stationServiceModelResponse:any;
  stationcontactModelRequest:stationcontactModelRequest;
  stationcontactModelResponse:stationcontactModelResponse[];
   // @ViewChildren(DataTableDirective)
  // datatableElement: QueryList<DataTableDirective>;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  dataTable: any;
  stationModelResponse:stationModelResponse[];
  constructor(private API: ApiService) {
    this.stationcontactModelRequest= new stationcontactModelRequest();
    this.stationcontactModelResponse=[];
    this.stationModelResponse=[];
  }

  ngOnInit(): void {
    this.getStationService();
    this.InitializeForm();
    this.getStationContactUs();
    this.getStation();
  }
  showhide(callfrm: string) {
    if (callfrm == "New") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = true;
      this.showeditButton = false;
      this.shownewButton = false;
    }
    if (callfrm == "Cancel") {
      this.submitted = false;
      this.addnewSurvey = false;
      this.showSurvey = true;
      this.showCancelButton = false;
      this.showSaveButton = false;
      this.showeditButton = false;
      this.shownewButton = true;
      this.stationContactForm.reset();
      this.stationContactForm.reset(this.stationContactForm.value);
    }
    if (callfrm == "Edit") {
      this.addnewSurvey = true;
      this.showSurvey = false;
      this.showCancelButton = true;
      this.showSaveButton = false;
      this.showeditButton = true;
      this.shownewButton = false;
    }
  }
  isActiveCheck(check: boolean) {
    if (check == true) {
      this.stationContactForm.controls.isActive.setValue(true);
    } else {
      this.stationContactForm.controls.isActive.setValue(false);
    }
  }
  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  get f() { return this.stationContactForm.controls; }
  InitializeForm(): any {
    this.stationContactForm = new FormGroup({
      contactId:new FormControl(""),
      contactName: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      stationServiceId: new FormControl(""),
      contactDesignation: new FormControl("",[Validators.required, this.noWhitespaceValidator]),
      contactAddress: new FormControl(""),
      contactPhone: new FormControl("",[Validators.required, Validators.pattern(".{10,10}")]),
      contactLocation: new FormControl(""),
      isActive:new FormControl(""),
      stationId:new FormControl(""),
    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  getStationService() {

      this.stationServiceModelResponse = [];
      this.API.getdata('/Menzies/getStationServiceList').subscribe(
        data => {
          if (data != null) {
            this.stationServiceModelResponse = data.StationServiceList;
          }
          else {
          }
        },
        error => {
          Swal.fire({
            text: error.error.Message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        });
    }

    addStationContactus() {
      //this.validations();

      this.submitted = true;
      //if (this.stationContactForm.valid == true) {
        if (this.stationContactForm.controls.isActive.value == "" || this.stationContactForm.controls.isActive.value == null) {
          this.stationContactForm.controls.isActive.setValue(false);
        }
        this.stationcontactModelRequest = this.stationContactForm.value;
        this.API.PostData('/Menzies/SaveContactUs', this.stationcontactModelRequest).subscribe(
          c => {
            if (c != null) {
              if(c.Status=="Failed"){
                Swal.fire({
                  text: 'StationContactus Already Exist.',
                  icon: 'error',
                  confirmButtonText: 'OK'
                });
              }
              else{
                Swal.fire({
                  text: 'StationContactus Saved Successfully',
                  icon: 'success',
                  confirmButtonText: 'OK'
                });
                this.getStationContactUs();
                this.showhide("Cancel");
              }

            }
          },
          error => {
            Swal.fire({
              text: error.error.Message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          });
      //}
    }

    getStationContactUs() {

        this.stationcontactModelResponse = [];
        this.API.getdata('/Menzies/getStationContactList').subscribe(
          data => {
            if (data != null) {
              this.stationcontactModelResponse = data.getcontactstationnwise;
            }
            else {
            }
          },
          error => {
            Swal.fire({
              text: error.error.Message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          });
      }

      editStationContactus(p) {
        window.scroll(0,0);
        this.showhide("Edit");
        this.stationContactForm.patchValue(p);
      }
      getStation() {
        this.stationModelResponse = [];
        this.API.getdata('/Menzies/getStationList_Web').subscribe(
          data => {
            if (data != null) {
              this.stationModelResponse = data.stationList;
            }
            else {
            }
          },
          error => {
            // Swal.fire({
            //   text: error.error.Message,
            //   icon: 'error',
            //   confirmButtonText: 'OK'
            // });
          });
      }
}
