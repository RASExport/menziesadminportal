import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './Pages/Category/category.component';
import { CertificationComponent } from './Pages/Certification/certification.component';
import { ClientComponent } from './Pages/Clients/client.component';
import { ContactstationstaffComponent } from './Pages/ContactStationStaff/contactstationstaff.component';
import { GeneralComponent } from './Pages/General/general.component';
import { LoginComponent } from './Pages/Login/login.component';
import { NewsComponent } from './Pages/News/news.component';
import { OfcContactComponent } from './Pages/OfcContacts/ofc-contact.component';
import { ServiceComponent } from './Pages/Services/service.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component'
import { StationcontactusComponent } from './Pages/StationContactUs/stationcontactus.component';
import { StationComponent } from './Pages/Stations/station.component';
import { StationservicesComponent } from './Pages/StationService/stationservices.component';
import { SubcategoryComponent } from './Pages/SubCategory/subcategory.component';
import { SubserviceComponent } from './Pages/SubServices/subservice.component';
import { SubstationserviceComponent } from './Pages/SubStationService/substationservice.component';
import { TeamComponent } from './Pages/Team/team.component';
import { Auth } from './Services/Guard/guard.service'

const routes: Routes = [
  {
    path: '', component: LayoutComponent,canActivate: [Auth],children: [
      {path:'service',component:ServiceComponent},
      {path:'category',component:CategoryComponent},
      {path:'general',component:GeneralComponent},
      {path:'client',component:ClientComponent},
      {path:'subCategory',component:SubcategoryComponent},
      {path:'news',component:NewsComponent},
      {path:'certificate',component:CertificationComponent},
      {path:'subservice',component:SubserviceComponent},
      {path:'stationservice',component:StationservicesComponent},
      {path:'substationservice',component:SubstationserviceComponent},
      {path:'stationcontactus',component:StationcontactusComponent},
      {path:'contactstationstaff',component:ContactstationstaffComponent},
      {path:'team',component:TeamComponent},
      {path:'ofcontact',component:OfcContactComponent},
      {path:'stations',component:StationComponent},
    ]
  },
  { path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
